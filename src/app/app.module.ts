import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes  } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PartnerModule } from './partner/partner.module';
import { PageLayoutComponent } from './_components/page-layout/page-layout.component';
import { LoginComponent } from './login/login.component';
import { LoginModule } from './login/login.module';
import { HomepageModule } from './homepage/homepage.module';
import { HomepageComponent } from './homepage/homepage.component';
import { NotFoundPageComponent } from './_components/not-found-page/not-found-page.component';

import { CommonService } from './_services/common.service';
import { ErrorInterceptor, JwtInterceptor, AuthGuard } from './_helpers';


const routers: Routes  = [
  {
    path: '',
    redirectTo: 'admin',
    pathMatch: 'full',
    canActivate: [AuthGuard],
  },
  {
    path: 'admin',
    component: HomepageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '**',
    component: NotFoundPageComponent,
  }
];
@NgModule({
  declarations: [
    AppComponent,
    PageLayoutComponent,
    NotFoundPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PartnerModule,
    HomepageModule,
    LoginModule,
    HttpClientModule,
    RouterModule.forRoot(routers),
  ],
  providers: [
    CommonService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
],
  bootstrap: [AppComponent]
})
export class AppModule { }
