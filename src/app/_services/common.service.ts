import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  private breadcrumb = new BehaviorSubject([]);
  currentBreadcrumb = this.breadcrumb.asObservable();

  constructor() { }

  changeBreadCrumb(items: any) {
    this.breadcrumb.next(items);
  }
}
