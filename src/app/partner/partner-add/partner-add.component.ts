import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-partner-add',
  templateUrl: './partner-add.component.html',
  styleUrls: ['./partner-add.component.scss']
})
export class PartnerAddComponent implements OnInit {
  constructor() { }

  @Output() changeBreadcrumb = new EventEmitter<any[]>();
  ngOnInit() {
    const breadcrumb: any[] = [
      {
        linkTo: 'admin',
        text: 'HOME',
      },
      {
        linkTo: 'partner/list',
        text: 'Partners',
      },
      {
        linkTo: 'partner/add',
        text: 'New partner',
        disable: true,
      },
    ];
    this.changeBreadcrumb.emit(breadcrumb);
  }
}
