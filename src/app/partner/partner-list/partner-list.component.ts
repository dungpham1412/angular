import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-partner-list',
  templateUrl: './partner-list.component.html',
  styleUrls: ['./partner-list.component.scss']
})
export class PartnerListComponent implements OnInit {

  constructor() { }

  @Output() changeBreadcrumb = new EventEmitter<any[]>();
  ngOnInit() {
    const breadcrumb: any[] = [
      {
        linkTo: 'admin',
        text: 'HOME',
      },
      {
        linkTo: 'partner/list',
        text: 'Partners',
        disable: true,
      },
      {
        linkTo: 'partner/add',
        text: 'Add',
      },
    ];
    this.changeBreadcrumb.emit(breadcrumb);
  }
}
