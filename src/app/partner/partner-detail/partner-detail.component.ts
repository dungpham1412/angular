import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-partner-detail',
  templateUrl: './partner-detail.component.html',
  styleUrls: ['./partner-detail.component.scss']
})
export class PartnerDetailComponent implements OnInit {
  constructor(private route: ActivatedRoute) { }

  @Output() changeBreadcrumb = new EventEmitter<any[]>();
  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    const breadcrumb: any[] = [
      {
        linkTo: 'admin',
        text: 'HOME',
      },
      {
        linkTo: 'partner/list',
        text: 'Partners',
      },
      {
        linkTo: `partner/detail/:${id}`,
        text: `Partner ${id}`,
        disable: true,
      },
    ];
    this.changeBreadcrumb.emit(breadcrumb);
  }
}
