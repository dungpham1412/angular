import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { PartnerComponent } from './partner.component';
import { PartnerListComponent } from './partner-list/partner-list.component';
import { PartnerAddComponent } from './partner-add/partner-add.component';
import { PartnerDetailComponent } from './partner-detail/partner-detail.component';


const routers: Routes = [
  {
    path: 'partner',
    component: PartnerComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        component: PartnerListComponent,
      },
      {
        path: 'add',
        component: PartnerAddComponent,
      },
      {
        path: 'detail/:id',
        component: PartnerDetailComponent,
      }
    ],
  }
];
@NgModule({
  declarations: [PartnerComponent, PartnerListComponent, PartnerAddComponent, PartnerDetailComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routers),
  ]
})
export class PartnerModule { }
