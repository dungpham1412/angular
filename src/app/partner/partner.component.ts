import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

// getBreadcrumb()
@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnerComponent implements OnInit {
  id = '';

  constructor(private cdr: ChangeDetectorRef) { }

  @Output() changeBreadcrumb = new EventEmitter<any[]>();
  ngOnInit() {
  }

  onActivate(componentReference) {
    // Below will subscribe to the searchItem emitter
    componentReference.changeBreadcrumb.subscribe((data) => {
      // Will receive the data from child here
      this.cdr.detectChanges();
      this.changeBreadcrumb.emit(data);
    });
  }

}
