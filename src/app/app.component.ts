import { Component } from '@angular/core';
import { AuthenticationService } from './_services/authentication.service';
import { User } from './_models/user';
import { PageLayoutComponent } from './_components/page-layout/page-layout.component';
import { LoginComponent } from './login/login.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AngularDemo';
  isLogged = false;

  constructor(private authenticationService: AuthenticationService) {
    this.isLogged = !!this.authenticationService.currentUserValue;
  }

  onActivate(componentReference) {
    if (componentReference instanceof LoginComponent) {
      componentReference.checkLogin.subscribe((data) => {
        this.isLogged = data;
      });
    }
  }
  checkLogin(isLogged) {
    this.isLogged = isLogged;
  }
}
