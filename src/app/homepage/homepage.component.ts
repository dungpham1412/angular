import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  breadcrumb: any[] = [
    {
      linkTo: 'admin',
      text: 'HOME',
    }
  ];
  menuConfig = [
    {
      text: 'PARTNER',
      number: '10',
      baseUrl: '/partner',
      icon: '',
      style: ''
    },
  ];
  constructor() { }

  @Output() changeBreadcrumb = new EventEmitter<any[]>();
  ngOnInit() {
    this.changeBreadcrumb.emit(this.breadcrumb);
  }

}
