import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { HomepageComponent } from './homepage.component';
import { PartnerModule } from '../partner/partner.module';
import { PartnerComponent } from '../partner/partner.component';

const routers: Routes = [
  {
    path: 'partner',
    component: PartnerComponent,
  },
];
@NgModule({
  declarations: [HomepageComponent],
  imports: [
    CommonModule,
    PartnerModule,
    RouterModule.forChild(routers),
  ]
})
export class HomepageModule { }
