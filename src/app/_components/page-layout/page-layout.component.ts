import { Component, OnInit, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { HomepageComponent } from '../../homepage/homepage.component';
import { LoginComponent } from '../../login/login.component';

import { AuthenticationService } from '../../_services/authentication.service';

@Component({
  selector: 'app-page-layout',
  templateUrl: './page-layout.component.html',
  styleUrls: ['./page-layout.component.scss']
})
export class PageLayoutComponent implements OnInit {
  currentUser = 'dungpt28';
  email = 'dungpt28@fsoft.com.vn';
  breadcrumb = [];
  isOpenPopup = false;

  @Output() loginEvent = new EventEmitter<boolean>();
  constructor(private authenticationService: AuthenticationService, private cdr: ChangeDetectorRef) {}

  ngOnInit() {
  }

  changeBreadcrumb($event) {
    this.breadcrumb = $event;
  }

  onActivate(componentReference) {
    if (!(componentReference instanceof LoginComponent)) {
      componentReference.changeBreadcrumb.subscribe((data) => {
        this.breadcrumb = data;
        this.cdr.detectChanges();
      });
    }
  }

  openPopupLogout() {
    this.isOpenPopup = !this.isOpenPopup;
  }
  logout() {
    this.authenticationService.logout();
    this.isOpenPopup = false;
    this.loginEvent.emit(false);
  }
}
